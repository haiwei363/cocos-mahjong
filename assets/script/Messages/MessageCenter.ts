import ManageBase from "../CptManage/ManageBase";
import Message, { MessagesType } from "./Message";

const { ccclass, property } = cc._decorator;

@ccclass
export default class MessageCenter {
    // 管理类列表
    static ManagerList: ManageBase[] = []

    // 发送消息
    static SendMessage(msg: Message) {
        for (let manager of this.ManagerList) {
            manager.ReceiveMessage(msg);
        }
    }

    static SendCustomMessage(type: MessagesType, content: any) {
        let msg = new Message(type, content);
        this.SendMessage(msg);
    }

}
