const { ccclass, property } = cc._decorator;

export enum MessagesType {
    table_timer_msg = 1,
    table_xiang_msg = 2,
}

@ccclass
export default class Messages {
    MsgType: number;
    Content: any;

    constructor(msgType, content) {
        this.MsgType = msgType;
        this.Content = content;
    }
}
