const { ccclass, property } = cc._decorator;

enum Xiang {
    dong = 0,
    nan,
    xi,
    bei
}

@ccclass
export default class XiangManage extends cc.Component {


    // start() {}

    // update (dt) {}

    public SetMyXiang(xiang: number) {
        let timerEx = this.node;
        switch (xiang) {
            case 0:
                timerEx.setRotation(0);
                break;
            case 1:
                timerEx.setRotation(90);
                break;
            case 2:
                timerEx.setRotation(180);
                break;
            case 3:
                timerEx.setRotation(-90);
                break;
        }
    }
}
