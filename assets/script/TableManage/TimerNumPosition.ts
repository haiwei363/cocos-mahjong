export default class TimerNumPosition {
    leftNum0: cc.Vec2 = cc.v2(-11.436, 0);
    leftNum1: cc.Vec2 = cc.v2(-3.434, 0);
    leftNum2: cc.Vec2 = cc.v2(-11.436, 0);
    leftNum3: cc.Vec2 = cc.v2(-10.115, 0);
    leftNum4: cc.Vec2 = cc.v2(-11.173, 0);
    leftNum5: cc.Vec2 = cc.v2(-11.173, 0);
    leftNum6: cc.Vec2 = cc.v2(-11.173, 0);
    leftNum7: cc.Vec2 = cc.v2(-10.115, 1.586);
    leftNum8: cc.Vec2 = cc.v2(-11.173, 0);
    leftNum9: cc.Vec2 = cc.v2(-11.173, 0);
    rightNum0: cc.Vec2 = cc.v2(11.641, 0)
    rightNum1: cc.Vec2 = cc.v2(20.013, 0)
    rightNum2: cc.Vec2 = cc.v2(12.17, 0)
    rightNum3: cc.Vec2 = cc.v2(13.227, 0)
    rightNum4: cc.Vec2 = cc.v2(11.905, 0)
    rightNum5: cc.Vec2 = cc.v2(11.905, 0)
    rightNum6: cc.Vec2 = cc.v2(11.905, 0)
    rightNum7: cc.Vec2 = cc.v2(13.227, 1.586)
    rightNum8: cc.Vec2 = cc.v2(11.905, 0)
    rightNum9: cc.Vec2 = cc.v2(11.905, 0)
}
