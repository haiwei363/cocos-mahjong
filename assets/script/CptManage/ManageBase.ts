import Messages, { MessagesType } from "../Messages/Message";
import MessageCenter from "../Messages/MessageCenter";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ManageBase extends cc.Component {

    onLoad() {
        MessageCenter.ManagerList.push(this);
    }

    ReceiveMessage(message: Messages) {

    }
}
